# Découverte du monde des backups

Vous avez très probablement une base de données mysql ou mariadb sur votre ordinateur. 
Le cas échéant, concevez-en une rapidement.

## Concevez un script de backup
Réalisez un script bash ou bat permettant de réaliser un export automatique de votre base de données.

Le backup devra être un fichier sql , nommé en fonction de la base de données exportée et contenant également la date et heure.

ex : `mydb_20230512_full_backup.sql`

Après l'export, Le fichier devra être backupé dans un dossier de votre choix.

Essayez de ne pas "hardcoder" les identifiants de connection de votre base de donnée ou encore le dossier cible.

## Automatisez la tache

Puis, réalisez une tache planifiée pour réaliser une sauvegarde complète de votre base à intervalle régulière.
Au sein d'un environnement linux, vous devrez utilisez cron.